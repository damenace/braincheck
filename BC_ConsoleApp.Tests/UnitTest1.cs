using System;
using System.IO;
using System.Collections.Generic;
using Xunit;
using BC_ConsoleApp;

namespace BC_ConsoleApp.Tests
{
    public class UnitTest1
    {
        [Fact]
        public void TestAdd()
        {
            List<string> files = new List<string>(Directory.GetFiles(@"../../../../BC_ConsoleApp/files", "*", SearchOption.AllDirectories));
            string[] input = { "../../../../BC_ConsoleApp/files/apple2.txt"};
            List<string> lines = new List<string>(input);
            MMCLI.Add("a*.txt", ref files, ref lines);
            Assert.Contains("../../../../BC_ConsoleApp/files/apple1.txt", lines);
            Assert.Contains("../../../../BC_ConsoleApp/files/apple2.txt", lines);
        }
        [Fact]
        public void TestRemove()
        {
            string[] input = { "../../../../BC_ConsoleApp/files/apple1.txt",
                               "../../../../BC_ConsoleApp/files/apple2.txt"};
            List<string> lines = new List<string>(input);
            MMCLI.Remove("*1.txt", ref lines);
            Assert.DoesNotContain("../../../../BC_ConsoleApp/files/apple1.txt", lines);
            Assert.Contains("../../../../BC_ConsoleApp/files/apple2.txt", lines);
        }
        [Fact]
        public void TestList()
        {
            string[] input = { "./files/apple2.txt",
                               "./files/orange2.txt" };
            List<string> lines = new List<string>(input);
            MMCLI.List(ref lines);
        }
        [Fact]
        public void TestWrite()
        {
            string[] input = { "../../../../BC_ConsoleApp/files/apple1.txt",
                               "../../../../BC_ConsoleApp/files/apple2.txt",
                               "../../../../BC_ConsoleApp/files/orange2.txt"};
            List<string> lines = new List<string>(input);

            MMCLI.Write("./application.man", ref lines);
            Assert.Contains("../../../../BC_ConsoleApp/files/apple1.txt", lines);
            Assert.Contains("../../../../BC_ConsoleApp/files/apple2.txt", lines);
            Assert.DoesNotContain("../../../../BC_ConsoleApp/files/orange2.txt", lines);

            var linesIn = new List<string>(File.ReadAllLines("./application.man"));
            Assert.Contains("../../../../BC_ConsoleApp/files/apple1.txt", linesIn);
            Assert.Contains("../../../../BC_ConsoleApp/files/apple2.txt", linesIn);
            Assert.DoesNotContain("../../../../BC_ConsoleApp/files/orange2.txt", linesIn);
        }
    }
}
