﻿using System;
using System.Collections.Generic;
using System.IO;

namespace BC_ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            var command = Console.ReadLine();
            List<string> split = new List<string>(command.Split(' '));
            if (split[0] == "mmcli")
            {
                var path = split[1];

                // Read each line of the file into a string array. Each element
                // of the array is one line of the file.
                List<string> files = new List<string>(Directory.GetFiles(@"./files", "*", SearchOption.AllDirectories));
                List<string> lines = new List<string>();
                bool exist = File.Exists(path);
                bool doWrite = false;

                if (exist)
                {
                    lines = new List<string>(File.ReadAllLines(@path));
                }
                else
                {
                    Console.WriteLine("Manifest does not exist!");
                }

                if (split.Contains("-d") && exist)
                {
                    doWrite = MMCLI.Remove(split[split.IndexOf("-d") + 1], ref lines);
                }

                if (split.Contains("-a"))
                {
                    if (!exist)
                    {
                        Console.WriteLine("Creating manifest!");
                        exist = true;
                    }
                    doWrite = MMCLI.Add(split[split.IndexOf("-a") + 1], ref files, ref lines);
                }

                if (doWrite || (split.Contains("-c") && exist))
                {
                    MMCLI.Write(path, ref lines);
                }

                if (split.Contains("-l") && exist)
                {
                    MMCLI.List(ref lines);
                }

                if (split.Contains("-h"))
                {
                    Console.WriteLine("-a <glob>    Add all files matching the glob to the target manifest.");
                    Console.WriteLine("-d <glob>    Remove all files matching the glob from target manifest.");
                    Console.WriteLine("-c		    Clean up the file by removing all paths in it that do not exist.");
                    Console.WriteLine("-l		    Shows the content of the manifest file");
                    Console.WriteLine("-h		    Print this content");
                }
            }

            Console.WriteLine("Press any key to exit.");
            Console.ReadKey();
        }
    }
}
