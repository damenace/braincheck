﻿using System;
using System.Collections.Generic;
using System.IO;

namespace BC_ConsoleApp
{
    static public class MMCLI
    {
        static public bool Remove(string glob, ref List<string> lines)
        {
            bool hasChange = false;
            try
            {
                foreach (string line in lines)
                {
                    if (Glob.Glob.IsMatch(line, glob))
                    {
                        hasChange = true;
                        lines.Remove(line);
                    }
                }
            }
            catch
            {
                Console.WriteLine("Manifest remove error!");
            }

            return hasChange;
        }

        static public bool Add(string glob, ref List<string> files, ref List<string> lines)
        {
            bool hasChange = false;
            try
            {
                foreach (string file in files)
                {
                    if (Glob.Glob.IsMatch(file, glob))
                    {
                        if (!lines.Contains(file.Replace('\\', '/')))
                        {
                            hasChange = true;
                            lines.Add(file.Replace('\\', '/'));
                        }
                    }
                }
            }
            catch
            {
                Console.WriteLine("Manifest add error!");
            }

            return hasChange;
        }

        static public void List(ref List<string> lines)
        {
            try
            {
                // Display the file contents by using a foreach loop.
                Console.WriteLine("Contents of manifest = ");
                Console.WriteLine("----------");
                foreach (string line in lines)
                {
                    Console.WriteLine(line);
                }
                Console.WriteLine("----------");
            }
            catch
            {
                Console.WriteLine("Manifest display error!");
            }
        }

        static public void Write(string path, ref List<string> lines)
        {
            try
            {
                List<string> templines = new List<string>();
                templines.AddRange(lines);
                using (var stream = new FileStream(path, FileMode.Create, FileAccess.Write))
                {
                    var writer = new StreamWriter(stream);
                    foreach (string line in lines)
                    {
                        if (File.Exists(line))
                        {
                            writer.WriteLine(line);
                            writer.Flush();
                        }
                        else
                        {
                            string path2 = Directory.GetCurrentDirectory();
                            Console.WriteLine(path2);
                            Console.WriteLine(line);
                            templines.Remove(line);
                        }
                    }
                }
                lines = templines;
            }
            catch
            {
                Console.WriteLine("Manifest write error!");
            }
        }
        static public void Write2(string path, ref List<string> lines)
        {
            try
            {
                using (var stream = new FileStream(path, FileMode.Create, FileAccess.Write))
                {
                    var writer = new StreamWriter(stream);
                    foreach (string line in lines)
                    {
                        writer.WriteLine(line);
                    }
                    writer.Flush();
                }
            }
            catch
            {
                Console.WriteLine("Manifest write error!");
            }
        }
    }
}
